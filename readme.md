#1 Please set up your database credentials on .env file
	-DATABASE_URL="postgresql://username:password@databaseIP:port/postgres?serverVersion=13&charset=utf8"
#2 Please run migration command:
	-php bin/console doctrine:migrations:migrate
#3 Start page:
	-symfony server:start