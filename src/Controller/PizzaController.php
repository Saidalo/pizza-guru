<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Pizza;
use App\Entity\Ingredient;
use Doctrine\Common\Persistence\ManagerRegistry;

class PizzaController extends AbstractController
{
    /**
     * @Route("/pizza", name="app_pizza")
     */
    public function index(): Response
    {

        $urls = [
            'https://media.istockphoto.com/photos/pizza-margarita-with-cheese-top-view-isolated-on-white-background-picture-id1168754685?k=20&m=1168754685&s=612x612&w=0&h=XauWtcSx53Ey0yswLWggNGQi7gmTjba0TXloXO4NAIs=',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9Z5Xz2ozgk45biJb9inK0q6BeUL8_JsvWAuddxd-a8Wuu7Nv2kLoNf1SI3rEMEoojXWc&usqp=CAU',
            'https://image.shutterstock.com/z/stock-photo-overhead-studio-shot-of-pepperoni-pizza-isolated-on-white-1133279930.jpg',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzgzRiMCnxs0aW5L6Eqq7fCkTuHiupmeuSQ69OBUvCnn2HJ-xcq6lrZulxJH0GBbVhhKc&usqp=CAU'
        ];
        $doctrine = $this->getDoctrine()->getManager();
        $pizzas = array_map(function($pizza) use ($urls) {
            $key = array_rand($urls);
            return ['body' => $pizza, 'pic' => $urls[$key]];
        },$doctrine->getRepository(Pizza::class)->findAll());
        return $this->render('pizza/index.html.twig', [
            'pizzas' => $pizzas,
            'ingredients' => $doctrine->getRepository(Ingredient::class)->getAll()
        ]);
    }

    /**
     * @Route("/createPizza", name="create_pizza")
     */
    public function createPizza(Request $request): Response
    {
        $ingredients = $request->request->get('ingredient-ids');
        $entityManager = $this->getDoctrine()->getManager();
        $price = $entityManager->getRepository(Ingredient::class)->getPriceByIds($ingredients);

        $product = new Pizza();
        $product->setName($request->request->get('name'));
        $product->setPrice($price['price']);
        $product->setIngredients($ingredients);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();
        return new JsonResponse([
            'success' => true,
        ],
        Response::HTTP_OK
        );
    }

    /**
     * @Route("/deletePizza", name="delete_pizza")
     */
    public function deletePizza(Request $request): Response
    {
        $id = $request->request->get('id');
        $entityManager = $this->getDoctrine()->getManager();
        $pizza = $entityManager->getRepository(Pizza::class)->find($id);
        $entityManager->remove($pizza);
        $entityManager->flush();
        return new JsonResponse(['success' => true], Response::HTTP_OK);
    }
}
