$('#add-ingredient').on('click', function() {

	if ($('.ingredient-container').length >= 1) {
		clone = $(this).closest('.ingredient-container').clone(true);
		if (clone.find('.svg-buttons').find('.svg-container').length == 1) {
			clone.find('.svg-buttons').append('<div class="svg-container remove"><svg viewbox="0 0 40 40" width="20" height="20"><path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" /></svg></div>');
		} 
		$("#ingredient-parent").append(clone);
	} else {
		$(this).closest('.ingredient-container').clone(true).appendTo('#ingredient-parent');
	}
});

$('body').delegate('.remove', 'click', function() {
	$(this).closest('.ingredient-container').remove();
});

$('.add-pizza').on('click', function() {
	let ids = document.getElementsByClassName('custom-select');
	ids = Array.prototype.map.call(ids, item => item.value);
	$.ajax({
      url: '/createPizza',
      type: 'POST',
      data: {
      	'name': $('#pizza-name').val(),
      	'ingredient-ids': ids.join()
      },
      success: function(data){
        if (data.success) {
        	$('#exampleModal').modal('hide');
        	location.reload();
        }
      }
    });
});

$('span.discount').on('click', function() {
	var pizza_id = $(this).attr('id');
	$(`#pizza-${pizza_id}`).remove();
	$.ajax({
      url: '/deletePizza',
      type: 'delete',
      data: {
      	'id': pizza_id
      },
      success: function(data){
        if (data.success) {

        }
      }
    });
});

$("body").on("click", "#up", function(){
	var part = $(this).closest('.ingredient-container');
	var index = $('div.ingredient-container').index(part);
	if(index > 0) {
		rows = $('#ingredient-parent .ingredient-container').remove();
		
		if ((index-1) == 0) {
			$(rows[index]).find('.remove').remove();
			$(rows[index-1]).find('.svg-buttons').append('<div class="svg-container remove"><svg viewbox="0 0 40 40" width="20" height="20"><path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" /></svg></div>');
		}
		temp = rows[index];
		rows[index] = rows[index-1];
		rows[index-1] = temp;

		rows.each(function(i, elem) {
			$('#ingredient-parent').append(elem);
		});
	}
});

$("body").on("click", "#down", function(){
	var part = $(this).closest('.ingredient-container');
	var index = $('div.ingredient-container').index(part);
	rows = $('#ingredient-parent .ingredient-container');
	if (index < (rows.length - 1)) {
		$('#ingredient-parent .ingredient-container').remove();
		if ((index) == 0) {
			$(rows[index]).find('.svg-buttons').append('<div class="svg-container remove"><svg viewbox="0 0 40 40" width="20" height="20"><path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30" /></svg></div>');
			$(rows[index+1]).find('.remove').remove();
		}
		temp = rows[index];
		rows[index] = rows[index+1];
		rows[index+1] = temp;

		rows.each(function(i, elem) {
			$('#ingredient-parent').append(elem);
		});
	}
});